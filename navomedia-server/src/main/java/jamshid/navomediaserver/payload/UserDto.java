package jamshid.navomediaserver.payload;

import jamshid.navomediaserver.entity.enums.AppRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDto {
    private UUID id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private AppRole appRole;
    private List<AppRole> appRoleList;
    private boolean enable;

    public UserDto(UUID id, String firstName, String lastName, String email, List<AppRole> appRoleList, boolean enable) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.appRoleList = appRoleList;
        this.enable = enable;
    }
}
