package jamshid.navomediaserver.service;

import jamshid.navomediaserver.entity.User;
import jamshid.navomediaserver.payload.ApiResponse;
import jamshid.navomediaserver.payload.ResToken;
import jamshid.navomediaserver.payload.SignIn;
import jamshid.navomediaserver.repository.UserRepository;
import jamshid.navomediaserver.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class AuthService implements UserDetailsService {

    private final UserRepository userRepository;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;
    private final JdbcTemplate jdbcTemplate;
    private final DtoService dtoService;

    @Autowired
    public AuthService(UserRepository userRepository, JwtTokenProvider jwtTokenProvider, AuthenticationManager authenticationManager, JdbcTemplate jdbcTemplate, DtoService dtoService) {
        this.userRepository = userRepository;
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManager = authenticationManager;
        this.jdbcTemplate = jdbcTemplate;
        this.dtoService = dtoService;
    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(() ->
                new UsernameNotFoundException("Username not found"));
    }
    public User getById(UUID id){
        return userRepository.findById(id).orElseThrow(() ->
                new IllegalStateException("User not found"));
    }


    public ResToken signIn(SignIn signIn) {
        try {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(signIn.getEmail(), signIn.getPassword())
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            User principal = (User) authentication.getPrincipal();
            String jwt = jwtTokenProvider.generateToken(principal);
            return new ResToken(jwt);
        }catch (Exception e){
            return null;
        }
    }

    public ApiResponse searchUser(String search) {
        List<Map<String, Object>> maps = jdbcTemplate.queryForList("select * from users where firstname like '%' + ? +'%' or lastname like '%' + ? +'%' or email like '%' + ? +'%'", search);
        return new ApiResponse("Ok",true, maps);
    }

    public ApiResponse all() {
        return new ApiResponse("Ok",true,userRepository.findAll().stream().map(dtoService::userDto).collect(Collectors.toList()));
    }
}
