package jamshid.navomediaserver.service;

import jamshid.navomediaserver.entity.User;
import jamshid.navomediaserver.payload.UserDto;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class DtoService {

    public UserDto userDto(User user){
        return new UserDto(
                user.getId(),
                user.getFirstname(),
                user.getLastname(),
                user.getEmail(),
                Collections.singletonList(user.getRole().getAppRole()),
                user.isEnabled()
        );
    }
}
