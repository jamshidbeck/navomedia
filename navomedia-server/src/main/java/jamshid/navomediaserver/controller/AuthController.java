package jamshid.navomediaserver.controller;

import jamshid.navomediaserver.payload.ResToken;
import jamshid.navomediaserver.payload.SignIn;
import jamshid.navomediaserver.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@Controller
@RequestMapping("/api/auth")
@CrossOrigin
public class AuthController {
    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }


    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody SignIn signIn) {
        ResToken resToken = authService.signIn(signIn);
        return ResponseEntity.status(resToken != null ? 200 : 401).body(resToken);
    }

    @GetMapping("/searchUser/{search}")
    public HttpEntity<?> searchUser(@PathVariable String search) {
        return ResponseEntity.ok(authService.searchUser(search));
    }

    @GetMapping("/all")
    public HttpEntity<?> searchUser() {
        return ResponseEntity.ok(authService.all());
    }
}
