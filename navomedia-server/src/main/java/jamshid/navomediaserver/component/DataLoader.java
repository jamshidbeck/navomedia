package jamshid.navomediaserver.component;

import jamshid.navomediaserver.entity.Role;
import jamshid.navomediaserver.entity.User;
import jamshid.navomediaserver.entity.enums.AppRole;
import jamshid.navomediaserver.repository.RoleRepository;
import jamshid.navomediaserver.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@Component
public class DataLoader implements CommandLineRunner {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    @Value("${spring.datasource.initialization-mode}")
    private String mode;

    @Autowired
    public DataLoader(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) throws Exception {
        if (mode.equals("always")){
            Role superAdmin = roleRepository.save(new Role(AppRole.ROLE_SUPER_ADMIN));
            Role admin = roleRepository.save(new Role(AppRole.ROLE_ADMIN));
            Role simpleUser = roleRepository.save(new Role(AppRole.ROLE_SIMPLE_USER));
            userRepository.save(new User(
                    "Jamshid",
                    "Umarov",
                    "jamshidbek.umarov08052gmail.com",
                    passwordEncoder.encode("08051437"),
                    superAdmin
            ));

        }
        else{
            userRepository.save(new User(
                    "admin",
                    "admin",
                    "test.test@gmail.com",
                    passwordEncoder.encode("root123"),
                    roleRepository.findByAppRole(AppRole.ROLE_ADMIN)
            ));
            userRepository.save(new User(
                    "user",
                    "simple",
                    "simple.user@gmail.com",
                    passwordEncoder.encode("simuspass"),
                    roleRepository.findByAppRole(AppRole.ROLE_SIMPLE_USER)
            ));
        }
    }
}
