package jamshid.navomediaserver.repository;

import jamshid.navomediaserver.entity.Role;
import jamshid.navomediaserver.entity.enums.AppRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByAppRole(AppRole appRole);

}
