package jamshid.navomediaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NavomediaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(NavomediaServerApplication.class, args);
    }

}
