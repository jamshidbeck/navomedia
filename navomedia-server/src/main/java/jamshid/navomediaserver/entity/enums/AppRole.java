package jamshid.navomediaserver.entity.enums;

public enum AppRole {
    ROLE_SIMPLE_USER,
    ROLE_ADMIN,
    ROLE_SUPER_ADMIN
}
