package jamshid.navomediaserver.entity;

import jamshid.navomediaserver.entity.enums.AppRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
public class Role implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public Role(AppRole appRole) {
        this.appRole = appRole;
    }

    @Column(unique = true)
    @Enumerated
    private AppRole appRole;



    @Override
    public String getAuthority() {
        return appRole.name();
    }
}
